# EasyRichtext

#### 介绍

> 简洁、简单、使用方便、基于Layui框架的Markdown编辑器。
> 因本人在使用layui过程中未发现Layui的Markdown编辑器因此有了自己开一个基于LayuiMarkdown编辑器的想法。
> 也希望大家在使用过程中，发现问题与希望添加的功能，在下方留言。
